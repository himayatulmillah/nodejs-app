// import module express router
const router = require('express').Router()

// import group router
const homeRouter = require('./homeRouter')
const articleRouter = require('./articleRouter')
const userRouter = require('./userRouter')

// definisi endpoint terhadap group router
router.use('/', homeRouter)
router.use('/articles', articleRouter)
router.use('/', userRouter)

// export router module
module.exports = router