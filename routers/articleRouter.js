// import module express router
const router = require('express').Router()

// import group router
const articleController = require('../controllers/articleController')

// definisi endpoint terhadap group router
router.get('/', articleController.index)

router.get('/create', articleController.createArticle)
router.post('/create', articleController.create) // submit form

router.get('/:id', articleController.detail) // detail article

router.patch('/update/:id', articleController.update)
router.delete('/delete/:id', articleController.delete)

// export router module
module.exports = router