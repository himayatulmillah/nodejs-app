const passport = require('passport')
const { Strategy: JwtStrategy, EctractJwt, ExtractJwt } = require('passport-jwt')
const { User } = require('../models')
const auth = require('../controllers/authController')

const options = {
    jwtFromRequest : ExtractJwt.fromHeader('authorization'),
    secretOrKey : 'BelajarPassportJWT'
}

passport.use(new JwtStrategy(options, async (payload, done) => {
    User.findByPk(payload.id)
        .then(user => done(null, user))
        .catch(err => done(err, false))
}))

module.exports = passport