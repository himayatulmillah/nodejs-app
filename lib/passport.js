const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const { User } = require('../models')
const auth = require('../controllers/authController')

// Fungsi untuk authentication
async function authenticate(username, password, done) {
    try {
        // memanggil method doLogin pada controller
        const user = await auth.doLogin({ username, password })
        console.log("username yang login => " + user.username)
        return done(null, user)
    }
    catch(err) {
        // parameter ketiga akan dilempar ke dalam flash
        return done(null, false, { message: err.message })
    }
}

// Cara untuk membuat sesi dan menghapus sesi
passport.use(
    new LocalStrategy({ usernameField: 'username', passwordField: 'password' }, authenticate)
)

passport.serializeUser(
    (user, done) => done(null, user.id)
)
passport.deserializeUser(
    async (id, done) => done(null, await User.findByPk(id))
)

// export module yang akan digunakan sebagai middleware
module.exports = passport