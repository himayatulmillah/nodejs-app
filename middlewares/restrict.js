module.exports = (req, res, next) => {
    // jika request berasal dari user yang terautentikasi,
    // maka proses akan menjalankan handler berikutnya
    console.log(req.user)
    if (req.isAuthenticated()) return next()
    // jika tidak, redirect ke halaman login
    res.redirect('/login')
}