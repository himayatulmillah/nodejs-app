const express = require('express')
const app = express()
const session = require('express-session')
const flash = require('express-flash')
const { PORT = 8000} = process.env

app.use(express.urlencoded({extended: false}))
app.use(express.json())

// setting passport local
// const passport = require('./lib/passport')
// app.use(passport.initialize())
// app.use(passport.session())

// setting passport JWT
const passport = require('./lib/passportJwt')
app.use(passport.initialize())

// setting flash
app.use(flash())

// setting view engine
app.set('view engine', 'ejs')

// setting session handler
app.use(session({
    secret: 'Buat ini jadi rahasia',
    resave: false,
    saveUninitialized: false
}))

// import router
const router = require('./routers')
app.use(router)

app.listen(PORT, () => console.log(`Server telah berjalan di http://localhost:${PORT}`))