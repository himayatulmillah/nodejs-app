const {Article} = require('../models')

module.exports = {
    index: (req, res) => {
        Article.findAll({
        }).then(article => {
            if (article.length !== 0 ) {
                res.render('./articles/index', {article})
            } else {
                res.json({
                    'status': 400,
                    'message': 'data kosong'
                })
            }
        })
    },
    create: (req, res) => {
        const {title, body} = req.body;

        Article.create({
            title,
            body,
            approved: false
        }).then(article => {
            res.render('./articles/detail', {article})
        })
    },
    update: (req, res) => {
        const articleId = req.params.id;
        const query = {
            where: {id: articleId}
        }
        
        Article.update({
            approved: true
        }, query).then(article => {
            res.json({
                'status': 200,
                'message': 'article approved!'
            })
        }).catch(err => {
            res.json({
                'status': 400,
                'message': 'gagal memperbarui data'
            })
        })
    },
    detail: (req, res) => {
        const articleId = req.params.id;

        Article.findOne({
            where: {id: articleId}
        }).then(article => {
            res.render('./articles/detail', {article})
        }).catch(err => {
            res.json({
                'status': 400,
                'message': 'data tidak ditemukan'
            })
        })
    },
    delete: (req, res) => {
        const articleId = req.params.id;

        Article.destroy({
            where: {id: articleId}
        }).then(article => {
            res.json({
                'status': 200,
                'message': 'article berhasil dihapus!'
            })
        }).catch(err => {
            res.json({
                'status': 400,
                'message': 'gagal menghapus article'
            })
        })
    },
    createArticle: (req, res) => {
        res.render('./articles/create')
    },
}