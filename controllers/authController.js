const {User} = require('../models')
const bycrpt = require('bcrypt')
const passport = require('passport')
const jwt = require('../lib/jwt')

function format (user) {
    const { id, username } = user
    return { id, username, accessToken : jwt.generateToken({id, username})}
}

module.exports = {
    register: (req, res) => {
        res.render('./users/register')
    },
    doRegister: (async(req, res) => {
        try {
            const {username} = req.body
            const encryptedPassword = bycrpt.hashSync(req.body.password, 10)
            const user = await User.create({
                username,
                password: encryptedPassword
            })
            return res.render('./users/login')
        }
        catch (err){
            console.log(err);
            return res.render('./register')
        }
    }),
    isLogin: passport.authenticate('local', {
        successRedirect: '/profile',
        failureRedirect: '/login',
        failureFlash: true // mengaktifkan flash
    }),
    login: (req, res) => {
        res.render('./users/login')
    },
    doLogin: (async({username, password}) => {
        try {
            const user = await User.findOne({ where: { username }})
            if (!user) return console.log("User not found!")
            const isPasswordValid = bycrpt.compareSync(password, user.password)
            if (!isPasswordValid) return console.log("Wrong password")
            return Promise.resolve(user)
        }
        catch(err) {
            console.log(err)
            return Promise.reject(err)
        }
    }),
    doLoginJwt: async(req, res) => {
        try {
            const {username, password} = req.body

            const user = await User.findOne({ where: {username: username} })
            if (!user) return res.json({'status': 'gagal', 'message': 'username not found.'})

            const isPasswordValid = bycrpt.compareSync(password, user.password)
            if (!isPasswordValid) return res.json({'status': 'gagal', 'message': 'password incorrect.'})

            return res.json(format(user))
        }
        catch(err) {
            res.json({'status': 'gagal', 'message': err.message})
        }
    },
    profile: (req, res) => {
        res.render('./users/profile', req.user.dataValues)
    },
    profileJwt: (req, res) => {
        const user = req.user;
        res.render('./users/profile', {user})
    }
}
